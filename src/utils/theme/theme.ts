export const globalTheme = {
  palette: {
    'background.1': {
      light: '#FAFAFA',
      dark: '#030E15',
      main: '#030E15',
    },
    'tiles.1': {
      light: '#FFFFFF',
      dark: '#15252C',
      main: '#15252C',
    },
    'tiles.2': {
      light: '#797979',
      dark: '#38464B',
      main: '#38464B',
    },
    'typo.1': {
      light: '#4C5052',
      dark: '#EFEFEF',
      main: '#EFEFEF',
    },
    'typo.2': {
      light: '#FFFFFF',
      dark: '#FFFFFF',
      main: '#FFFFFF',
    },
    'typo.3': {
      light: '#99A0A0',
      dark: '#99A0A0',
      main: '#99A0A0',
    },
    'primary.1': {
      light: '#00A0FF',
      dark: '#00A0FF',
      main: '#00A0FF',
    },
    'primary.2': {
      light: '#21ACFF',
      dark: '#21ACFF',
      main: '#21ACFF',
    },
    'primary.3': {
      light: '#0088D9',
      dark: '#0088D9',
      main: '#0088D9',
    },
    'secondary.1': {
      light: '#F3F3F3',
      dark: '#F3F3F3',
      main: '#F3F3F3',
    },
    'secondary.2': {
      light: '#EEEDED',
      dark: '#2B3B40',
      main: '#2B3B40',
    },
    'secondary.3': {
      light: '#E8E8E8',
      dark: '#394B51',
      main: '#394B51',
    },
    'secondary.4': {
      light: '#E0E0E0',
      dark: '#1F2A2E',
      main: '#1F2A2E',
    },
    'green.1': {
      light: '#01C294',
      dark: '#01C294',
      main: '#01C294',
    },
    'green.2': {
      light: '#00CC9B',
      dark: '#00CC9B',
      main: '#00CC9B',
    },
    'green.3': {
      light: '#00A880',
      dark: '#00A880',
      main: '#00A880',
    },
    'red.1': {
      light: '#DF4661',
      dark: '#DF4661',
      main: '#DF4661',
    },
    'red.2': {
      light: '#E94A66',
      dark: '#E94A66',
      main: '#E94A66',
    },
    'red.3': {
      light: '#CB3F58',
      dark: '#CB3F58',
      main: '#CB3F58',
    },
    'other.1': {
      light: '#D2BD00',
      dark: '#D2BD00',
      main: '#D2BD00',
    },
  },
  typography: {
    "fontFamily": `"Roboto", "Helvetica", "Arial", sans-serif`,
    h1: {
      fontFamily: 'Poppins',
      fontWeight: 600,
      fontSize: '24px',
      lineHeight: '36px',
    },
    h2: {
      fontFamily: 'Poppins',
      fontSize: '20px',
      fontWeight: 600,
      lineHeight: '30px',
    },
    h3: {
      fontFamily: 'Lato',
      fontWeight: 700,
      fontSize: '20px',
      lineHeight: '24px',
    },
    h4: {
      fontFamily: 'Poppins',
      fontWeight: 500,
      fontSize: '16px',
      lineHeight: '24px',
    },
    button: {
      fontFamily: 'Poppins',
      fontWeight: 600,
      fontSize: '14px',
      lineHeight: '21px',
    },
    body1: {
      fontFamily: 'Poppins',
      fontWeight: 500,
      fontSize: '14px',
      lineHeight: '21px',
    },
    body2: {
      fontFamily: 'Poppins',
      fontWeight: 500,
      fontSize: '12px',
      lineHeight: '18px',
    },
    body3: {
      fontFamily: 'Lato',
      fontWeight: 700,
      fontSize: '12px',
      lineHeight: '14px',
    },
    body4: {
      fontFamily: 'Lato',
      fontWeight: 600,
      fontSize: '12px',
      lineHeight: '14px',
    },
    caption: {
      fontFamily: 'Poppins',
      fontWeight: 500,
      fontSize: '12px',
      lineHeight: '15px',
    },
  },
  breakpoints: {
    values: {
      xs: 0,
      sm: 768,
      md: 992,
      lg: 1200,
      xl: 1680,
    },
  },
}
