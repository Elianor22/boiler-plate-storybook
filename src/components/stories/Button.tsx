import { Button } from "../atoms/Button/ButtonComponent";
import React from "react";
/* eslint-disable @typescript-eslint/no-explicit-any */

const ButtonComponent = ({
    variant,
    type,
    children,
    sx,
    width,
    isSubmit,
    className,
    onClick,
}: any) => {
    return (
        <Button
            variant={variant}
            type={type}
            sx={{ ...sx }}
            width={width}
            className={className}
            isSubmit={isSubmit}
            onClick={() => onClick()}
        >
            {children}
        </Button>
    );
};

export default ButtonComponent;
