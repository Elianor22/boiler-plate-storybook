import Icon from "../atoms/Icons";
import { IconsVariant } from "../../constants/icon";
import Typography from "../atoms/Typography";
import React from "react";

const IconComponent = () => {
    const icons: IconsVariant[] = [
        "burger",
        "arrowup",
        "arrowdown",
        "arrowleft",
        "arrowright",
        "plus",
    ];
    return (
        <div style={{ display: "flex", flexDirection: "column" }}>
            {icons.map((icon, idx) => (
                <div
                    key={idx}
                    style={{ marginBottom: "1rem", display: "flex" }}
                >
                    <Icon variant={icon} />
                    <Typography sx={{ ml: 1 }}>{icon}</Typography>
                </div>
            ))}
        </div>
    );
};

export default IconComponent;
