import { Grid } from "@mui/material";
import Typography from "../atoms/Typography";
import {ITypography} from "../atoms/Typography";
import React from "react";

const TypographyComp = ({ variant, weight, mb, mt, className, sx }: ITypography) => {
    return (
        <Grid container>
            <Grid item md={6}>
                <div style={{ display: "flex", flexDirection: "column" }}>
                    <Typography variant="h1">H1</Typography>
                    <Typography variant="h2">H2</Typography>
                    <Typography variant="h3">H3</Typography>
                    <Typography variant="h2">H4</Typography>
                    <Typography variant="body1">body 1</Typography>
                    <Typography variant="body2">body 2</Typography>
                    <Typography variant="body2">body 3</Typography>
                    <Typography variant="button">button</Typography>
                    <Typography variant="caption">caption</Typography>
                </div>
            </Grid>
            <Grid item md={6}>
                <div style={{ display: "flex", justifyContent: "start" }}>
                    <Typography
                        variant={variant}
                        weight={weight}
                        mb={mb}
                        mt={mt}
                        className={className}
                        sx={sx}
                    >
                        Lorem ipsum dolor sit amet consectetur adipisicing elit.
                        Iusto, at.
                    </Typography>
                </div>
            </Grid>
        </Grid>
    );
};

export default TypographyComp;
