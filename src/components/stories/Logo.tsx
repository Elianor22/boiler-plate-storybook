import Logo from "../atoms/logo";

const LogoComponent = () => <Logo />;

export default LogoComponent;
