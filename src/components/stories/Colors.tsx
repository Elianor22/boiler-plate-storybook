import { Box, useTheme } from '@mui/material'
import { ColorProps, ColorsVariant } from '../../constants/colors'
import Typography from '../atoms/Typography'
import React from 'react'

const Colors = () => {
  const primary: ColorsVariant[] = ['primary.1', 'primary.2', 'primary.3']
  const secondary: ColorsVariant[] = ['secondary.1', 'secondary.2', 'secondary.3']
  const typo: ColorsVariant[] = ['typo.1', 'typo.2', 'typo.3']
  const green: ColorsVariant[] = ['green.1', 'green.2', 'green.3']
  const red: ColorsVariant[] = ['red.1', 'red.2', 'red.3']
  const theme = useTheme()
  return (
    <div style={{ display: 'block' }}>
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            width: 300,
            marginTop: 16,
          }}
        >
          {primary.map((item, idx) => {
            return (
              <div
                key={idx}
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  paddingRight: 20,
                }}
              >
                <Box
                  sx={{
                    backgroundColor: ColorProps[item][theme.palette.mode],
                    height: 100,
                    width: 100,
                  }}
                />
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    margin: '0 1rem',
                    width: 140,
                  }}
                >
                  <Typography variant="h2" color="typo.1">
                    {item}
                  </Typography>
                  <Typography color="typo.1">{ColorProps[item][theme.palette.mode]}</Typography>
                </div>
              </div>
            )
          })}
        </div>

        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            width: 300,
            marginTop: 16,
          }}
        >
          {secondary.map((item, idx) => {
            return (
              <div
                key={idx}
                style={{
                  display: 'flex',
                  paddingRight: 20,
                  flexDirection: 'row',
                }}
              >
                <Box
                  sx={{
                    backgroundColor: ColorProps[item][theme.palette.mode],
                    height: 100,
                    width: 100,
                  }}
                />
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    margin: '0 1rem',
                    width: 140,
                  }}
                >
                  <Typography variant="h2" color="typo.1">
                    {item}
                  </Typography>
                  <Typography color="typo.1">{ColorProps[item][theme.palette.mode]}</Typography>
                </div>
              </div>
            )
          })}
        </div>

        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            width: 300,
            marginTop: 16,
          }}
        >
          {typo.map((item, idx) => {
            return (
              <div
                key={idx}
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  paddingRight: 20,
                }}
              >
                <Box
                  sx={{
                    backgroundColor: ColorProps[item][theme.palette.mode],
                    height: 100,
                    width: 100,
                  }}
                />
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    margin: '0 1rem',
                    width: 140,
                  }}
                >
                  <Typography variant="h2" color="typo.1">
                    {item}
                  </Typography>
                  <Typography color="typo.1">{ColorProps[item][theme.palette.mode]}</Typography>
                </div>
              </div>
            )
          })}
        </div>

        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            width: 300,
            marginTop: 16,
          }}
        >
          {green.map((item, idx) => {
            return (
              <div
                key={idx}
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  paddingRight: 20,
                }}
              >
                <Box
                  sx={{
                    backgroundColor: ColorProps[item][theme.palette.mode],
                    height: 100,
                    width: 100,
                  }}
                />
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    margin: '0 1rem',
                    width: 140,
                  }}
                >
                  <Typography variant="h2" color="typo.1">
                    {item}
                  </Typography>
                  <Typography color="typo.1">{ColorProps[item][theme.palette.mode]}</Typography>
                </div>
              </div>
            )
          })}
        </div>

        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            width: 300,
            marginTop: 16,
          }}
        >
          {red.map((item, idx) => {
            return (
              <div
                key={idx}
                style={{
                  display: 'flex',
                  paddingRight: 20,
                  flexDirection: 'row',
                }}
              >
                <Box
                  sx={{
                    backgroundColor: ColorProps[item][theme.palette.mode],
                    height: 100,
                    width: 100,
                  }}
                />
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    margin: '0 1rem',
                    width: 140,
                  }}
                >
                  <Typography variant="h2" color="typo.1">
                    {item}
                  </Typography>
                  <Typography color="typo.1">{ColorProps[item][theme.palette.mode]}</Typography>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}

export default Colors
