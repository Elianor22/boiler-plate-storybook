import React from 'react'
import InputText from '../molecules/InputField/InputText/InputText'

const InputField = () => {
  return <InputText label="Input text" />
}

export default InputField
