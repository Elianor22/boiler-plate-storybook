/* eslint-disable @typescript-eslint/no-explicit-any */
import { Box } from "@mui/material";
import Card from "../molecules/Card";
import {ICard} from '../../interface/card'
import React from "react";

const CardComponent = ({ title, titleVariant, children, sxCard }: ICard) => {
    return (
        <Card title={title} sxCard={sxCard} titleVariant={titleVariant}>
            <Box sx={{ display: "flex", flexDirection: "column" }}>
                {children}
                
                <Box sx={{pt:1}}>
                    this is rendering in card not from child you can pass children props
                </Box>
            </Box>
        </Card>
    );
};
export default CardComponent;
