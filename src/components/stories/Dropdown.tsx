import { Box } from '@mui/material'
import Dropdown from '../molecules/Dropdown'
import React from 'react'

const DropdownComponent = () => {
  const data = [
    { name: 'test 1', value: 'test1' },
    { name: 'test 2', value: 'test2' },
    { name: 'test 3', value: 'test3' },
  ]
  return (
    <Box sx={{ width: '200px' }}>
      <Dropdown options={data} label="Dropdown" labelId="dropdown" fullWidth />
    </Box>
  )
}

export default DropdownComponent
