import { Box } from "@mui/material";
import Typography from "../Typography";
import { Dot } from "./elements";

const Logo = () => {
    return (
        <div>
            <Box
                sx={{
                    display: "flex",
                    position: "relative",
                    alignItems: "center",
                }}
            >
                <Typography
                    variant="h1"
                    color="primary.1"
                    weight={700}
                    sx={{
                        display: "flex",
                        position: "relative",
                        mr: "10px",
                    }}
                >
                    MGP
                    <Dot />
                </Typography>
                <Typography variant="h3">Commerce</Typography>
            </Box>
        </div>
    );
};

export default Logo;
