import styled from "@emotion/styled";
import { ColorProps } from "../../../constants/colors";


export const LogoWrapper = styled('div')(() => ({
    width: '100%',
}))

export const Dot = styled('div')(() => ({
    height: 5,
    width: 5,
    backgroundColor: ColorProps['red.1']['light'],
    borderRadius: '100%',
    position:'absolute',
    bottom:8,
    right:'-5px'

    
}))

