import { Button as MuiButton, SxProps, useTheme } from '@mui/material'
import { ReactNode } from 'react'
import {
  buttonProps,
  typeButton,
  variantButton,
  // buttonType,s
} from '../../../constants/button'
import { ColorProps } from '../../../constants/colors'
import { TypographyStyle } from '../../../constants/typography'
import React from 'react'

export interface IButton {
  type?: typeButton
  href?: string
  onClick?: () => void
  className?: string
  isSubmit?: boolean
  width?: number
  sx?: SxProps
  variant?: variantButton
  children?: ReactNode
  block?: boolean
  // buttonType?: buttonType;
}

export const Button = ({
  type = 'default',
  variant = 'primary',
  onClick,
  className,
  isSubmit = false,
  block = false,
  width,
  sx,
  children,
}: // buttonType,
IButton) => {
  const theme = useTheme()
  const bgColor = ColorProps[buttonProps[variant][type]][theme.palette.mode]
  console.log(type)
  return (
    <MuiButton
      className={className}
      onClick={onClick}
      sx={{
        ...TypographyStyle['button'],
        boxShadow: 'none',
        width: block ? '100%' : `${width}px`,
        minWidth: `${width}px`,
        height: '36px',
        display: 'flex',
        alignItems: 'center',
        borderRadius: '4px',
        color: 'white',
        backgroundColor: bgColor,
        cursor: type === 'inactive' ? 'not-allowed' : 'visible',
        '&:hover': {
          backgroundColor: bgColor,
          boxShadow: 'none',
        },
        ...sx,
      }}
      disableRipple={type === 'inactive' ? true : false}
      type={isSubmit ? 'submit' : 'button'}
    >
      {children}
    </MuiButton>
  )
}
