import { ReactNode } from "react";
import { SxProps, Typography as MuiTypography, useTheme } from "@mui/material";
import { TypographyVariant } from "../../../constants/typography";
import { ColorProps, ColorsVariant } from "../../../constants/colors";

export interface ITypography {
    variant?: TypographyVariant;
    size?: string | number;
    className?: string;
    color?: ColorsVariant;
    sx?: SxProps;
    weight?: number;
    mb?: number;
    mt?: number;
    children?: ReactNode;
}
const Typography = ({
    variant,
    color = "typo.1",
    size,
    className,
    mb,
    mt,
    sx,
    weight,
    children,
}: ITypography) => {
    const theme = useTheme();
    return (
        <MuiTypography
            variant={variant}
            className={className}
            mt={mt}
            mb={mb}
            sx={{
                color: ColorProps[color][theme.palette.mode],
                fontSize: size,
                fontWeight: weight,
                ...sx,
            }}
        >
            {children}
        </MuiTypography>
    );
};

export default Typography;
