import { IconProps } from "../../../constants/icon";
import {
    Burger,
    ArrowUp,
    ArrowDown,
    ArrowLeft,
    ArrowRight,
    Plus,
} from "./assets";
import React from "react";

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const components: any = {
    burger: Burger,
    arrowup: ArrowUp,
    arrowdown: ArrowDown,
    arrowleft: ArrowLeft,
    arrowright: ArrowRight,
    plus: Plus,
};

const Icon = ({ variant, color, sx }: IconProps) => {
    if (components[variant]) {
        const IconComponent = components[variant];
        return <IconComponent color={color} sx={sx} />;
    }
    return null;
};

export default Icon;
