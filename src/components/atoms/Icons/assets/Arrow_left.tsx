import { SvgIcon } from "@mui/material";
import { singleIconProps } from "../../../../constants/icon";
import { ColorProps } from "../../../../constants/colors";

const ArrowLeft = ({ color = "typo.1", additionalCss }: singleIconProps) => {
    const theme = "light";
    const strokeColor = ColorProps[color][theme];
    return (
        <SvgIcon sx={{ ...additionalCss }}>
            <path
                d="M1 10.625L23 10.625M1 10.625L10.625 1M1 10.625L10.625 20.25"
                stroke={strokeColor}
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </SvgIcon>
    );
};

export default ArrowLeft;
