import { SvgIcon } from "@mui/material";
import { ColorProps } from "../../../../constants/colors";

import { singleIconProps } from "../../../../constants/icon";
import React from "react";

const Burger = ({ color = "typo.1", size, sx }: singleIconProps) => {
    const theme = "light";
    const strokeColor = ColorProps[color][theme];
    return (
        <SvgIcon sx={{ fontSize: size, ...sx }}>
            <path
                d="M3 3H21M3 21H21H3ZM3 12H21H3Z"
                stroke={strokeColor}
                strokeWidth="5"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </SvgIcon>
    );
};

export default Burger;
