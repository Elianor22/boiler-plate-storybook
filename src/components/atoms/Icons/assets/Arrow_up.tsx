import { SvgIcon } from "@mui/material";
import { singleIconProps } from "../../../../constants/icon";
import { ColorProps } from "../../../../constants/colors";

const ArrowUp = ({ color = "typo.1", additionalCss }: singleIconProps) => {
    const theme = "light";
    const strokeColor = ColorProps[color][theme];
    return (
        <SvgIcon sx={{ ...additionalCss }}>
            <path
                d="M11.0119 0.518273C11.122 0.358371 11.2693 0.227629 11.4412 0.137308C11.613 0.0469872 11.8042 -0.000205755 11.9984 -0.000205755C12.1925 -0.000205755 12.3837 0.0469872 12.5556 0.137308C12.7274 0.227629 12.8748 0.358371 12.9849 0.518273L23.7857 16.1195C23.9107 16.2994 23.984 16.5102 23.9977 16.7289C24.0113 16.9476 23.9648 17.1658 23.8631 17.3599C23.7614 17.554 23.6085 17.7165 23.4209 17.8298C23.2333 17.9431 23.0183 18.0028 22.7992 18.0024H1.19753C0.978923 18.0015 0.764699 17.9411 0.577896 17.8275C0.391092 17.714 0.238776 17.5516 0.137327 17.358C0.0358782 17.1644 -0.0108646 16.9467 0.00212512 16.7285C0.0151149 16.5103 0.0873458 16.2997 0.21105 16.1195L11.0119 0.518273Z"
                fill={strokeColor}
            />
        </SvgIcon>
    );
};

export default ArrowUp;
