import { SvgIcon } from "@mui/material";
import { singleIconProps } from "../../../../constants/icon";
import { ColorProps } from "../../../../constants/colors";
import React from "react";

const Plus = ({ color = "typo.1", additionalCss, sx }: singleIconProps) => {
    const theme = "light";
    const strokeColor = ColorProps[color][theme];
    return (
        <SvgIcon sx={{ additionalCss, ...sx }}>
            <path
                d="M12 1V23M23 12H1H23Z"
                stroke={strokeColor}
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </SvgIcon>
    );
};

export default Plus;
