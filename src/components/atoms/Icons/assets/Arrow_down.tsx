import { SvgIcon } from "@mui/material";
import { singleIconProps } from "../../../../constants/icon";
import { ColorProps } from "../../../../constants/colors";

const ArrowDown = ({ color = "typo.1", additionalCss }: singleIconProps) => {
    const theme = "light";
    const strokeColor = ColorProps[color][theme];
    return (
        <SvgIcon sx={{ ...additionalCss }}>
            <path
                d="M11.0119 17.4842C11.122 17.6441 11.2693 17.7748 11.4412 17.8651C11.613 17.9555 11.8042 18.0026 11.9984 18.0026C12.1925 18.0026 12.3837 17.9555 12.5556 17.8651C12.7274 17.7748 12.8748 17.6441 12.9849 17.4842L23.7857 1.88295C23.9107 1.703 23.984 1.49224 23.9977 1.27355C24.0113 1.05486 23.9648 0.836613 23.8631 0.642523C23.7614 0.448433 23.6085 0.285921 23.4209 0.172643C23.2334 0.0593663 23.0183 -0.00034364 22.7992 1.4877e-06H1.19753C0.978923 0.000904449 0.764699 0.0613822 0.577896 0.174931C0.391092 0.288479 0.238776 0.450802 0.137327 0.644444C0.0358782 0.838085 -0.0108646 1.05572 0.00212512 1.27394C0.0151149 1.49216 0.0873458 1.70271 0.21105 1.88295L11.0119 17.4842Z"
                fill={strokeColor}

            />
        </SvgIcon>
    );
};

export default ArrowDown;
