import { SvgIcon } from "@mui/material";
import { singleIconProps } from "../../../../constants/icon";
import { ColorProps } from "../../../../constants/colors";
import React from "react";

const ArrowRight = ({ color = "typo.1", additionalCss }: singleIconProps) => {
    const theme = "light";
    const strokeColor = ColorProps[color][theme];
    return (
        <SvgIcon sx={{ ...additionalCss }}>
            <path
                d="M23 10.625L1 10.625M23 10.625L13.375 1M23 10.625L13.375 20.25"
                stroke={strokeColor}
                strokeWidth="2"
                strokeLinecap="round"
                strokeLinejoin="round"
            />
        </SvgIcon>
    );
};

export default ArrowRight;
