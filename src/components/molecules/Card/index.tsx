import { Box, SxProps } from "@mui/material";
import React from "react";
import { TypographyVariant } from "../../../constants/typography";
import Typography from "../../atoms/Typography";
import { MuiCard, CardHeader } from "./elements";
import {ICard} from "../../../interface/card";

const Card = ({
    children,
    width,
    bgColor,
    title,
    titleVariant = "h2",
    sxTitle,
    sxCard,
    sxHeader,
}: ICard) => {
    return (
        <MuiCard width={width} bgColor={bgColor} sx={sxCard}>
            {title && (
                <CardHeader style={sxHeader}>
                    <Typography variant={titleVariant} sx={sxTitle}>
                        {title}
                    </Typography>
                </CardHeader>
            )}
            <Box>{children}</Box>
        </MuiCard>
    );
};

export default Card;
