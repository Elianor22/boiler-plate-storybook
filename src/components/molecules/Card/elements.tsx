import styled from "@emotion/styled";
import { Card } from "@mui/material";
import { Spacing } from "../../../constants/spacing";

interface ICardProps {
    width?: String | Number;
    bgColor?: String;
    sx?: any;
}
export const MuiCard = styled(Card)(({ sx, bgColor, width }: ICardProps) => ({
    padding: "1rem",
    boxShadow: " rgba(0, 0, 0, 0.24) 0px 3px 8px",
    backgroundColor: bgColor,
    width: width,
    ...sx,
}));
export const CardHeader = styled("div")(({ style }) => ({
    display: "block",
    marginBottom: Spacing["md"],
    ...style,
}));
