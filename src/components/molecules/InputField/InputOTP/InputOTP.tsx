import React, { useCallback } from 'react'
import { SingleInput } from '../elements'

const InputOTP = () => {
  const handleChange = (e: any) => {
    console.log(e.target.value)
  }

  return <SingleInput maxLength={1} onChange={(e: any) => handleChange(e)} />
}

export default InputOTP
