import styled from '@emotion/styled'
import { TextField } from '@mui/material'
import { ColorProps } from '../../../constants/colors'
import { Spacing } from '../../../constants/spacing'
import { TypographyStyle } from '../../../constants/typography'

export const InputStyle = styled(TextField)(() => ({
  width: '100%',
  borderRadius: '4px',
  '& .MuiOutlinedInput-root': {
    '&:hover fieldset': {
      borderColor: ColorProps['secondary.1']['light'],
    },
    '&.Mui-focused fieldset': {
      borderColor: ColorProps['secondary.1']['light'],
    },
  },
}))

export const SingleInput = styled('input')(() => ({
  height: 45,
  width: 45,
  borderRadius: '4px',
  color: '#000',
  backgroundColor: ColorProps['secondary.1']['light'],
  paddingLeft: Spacing['sm'],
  paddingRight: Spacing['sm'],
  textAlign: 'center',
  ...TypographyStyle['h2']
}))
