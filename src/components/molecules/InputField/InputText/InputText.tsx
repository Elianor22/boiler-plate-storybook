import { InputStyle } from '../elements'
import { IInputField } from '../../../../constants/inputField'
import React from 'react'

const InputText = ({
  variant,
  label,
  name,
  value,
  id,
  size = 'small',
  type,
  onChange,
  onBlur,
  sx,
  disabled,
}: IInputField) => {
  return (
    <InputStyle
      id={id}
      label={label}
      size={size}
      value={value}
      name={name}
      type={type}
      variant={variant}
      onBlur={(e) => onBlur?.(e.target.value)}
      onChange={(e: React.ChangeEvent<HTMLInputElement>) => onChange?.(e.target.value)}
      sx={{ ...sx }}
      disabled={disabled}
    />
  )
}

export default InputText
