import styled from '@emotion/styled'
import { InputLabel } from '@mui/material'

export const LabelStyled = styled(InputLabel)(() => ({
  '& .Mui-focused fieldset': {
    color: 'red',
  },
}))
