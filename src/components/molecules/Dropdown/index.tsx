import * as React from 'react'
import MenuItem from '@mui/material/MenuItem'
import FormControl from '@mui/material/FormControl'
import Select from '@mui/material/Select'
import { IDropdown } from '../../../constants/dropdown'
import {LabelStyled} from './elements'

const index = ({ value, name, options, id, label, onChange, sx, sxLabel, fullWidth = false , labelId}: IDropdown) => {
  const [values, setValues] = React.useState(value)

  return (
    <FormControl fullWidth={fullWidth} sx={{ ...sx }}>
      <LabelStyled id={id} sx={{ ...sxLabel }}>
        {label}
      </LabelStyled>
      <Select
        labelId={labelId}
        id={id}
        value={values}
        name={name}
        onChange={(e) => {
          setValues(e.target.value)
          onChange?.(e.target.value)
        }}
        sx={{ ...sx }}
      >
        <MenuItem disabled value="">
          {name}
        </MenuItem>
        {options.map((item, idx) => (
          <MenuItem key={idx} value={item.value}>
            {item.name}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  )
}

export default index
