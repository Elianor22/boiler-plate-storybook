/* eslint-disable @typescript-eslint/ban-types */
import { SxProps } from "@mui/material"

export interface IOptions {
    value?: string
    name?: string
}
export interface IDropdown {
    value?: string
    name?: string
    id?: string
    options: Array<IOptions>
    label?: string
    onChange?: Function
    sx?: SxProps
    sxLabel?: SxProps
    fullWidth?: boolean
    labelId?:string
}
