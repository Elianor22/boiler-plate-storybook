/* eslint-disable @typescript-eslint/no-explicit-any */

import { SxProps } from "@mui/material"
export type InputVarian = "filled" | "standard" | "outlined"
export type InputSize = "small" | "medium"
export type InputType = "text" | "number" | "date" | "file"

/* eslint-disable @typescript-eslint/ban-types */
export interface IInputField {
    variant?: InputVarian
    label?: string
    name?: string
    value?: any
    id?: string
    size?: InputSize
    type?: InputType
    onChange?: Function
    placeholder?: string
    onBlur?: Function
    sx?: SxProps
    disabled?:boolean
}