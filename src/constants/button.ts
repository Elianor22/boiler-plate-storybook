import { ColorsVariant } from './colors'

export type variantButton = 'primary' | 'secondary' | 'green' | 'red' | 'outline'
export type typeButton = 'default' | 'inactive' | 'hover' | 'active'
export type labelButton = 'primary button' | 'secondary button' | 'outline button'

interface IButtonVariant {
  primary: IButtonTypes
  secondary: IButtonTypes
  green: IButtonTypes
  red: IButtonTypes
  outline: IButtonTypes
}

interface IButtonTypes {
  default: ColorsVariant
  hover: ColorsVariant
  active: ColorsVariant
  inactive: ColorsVariant
}

export const buttonProps: IButtonVariant = {
  primary: {
    default: 'primary.1',
    hover: 'primary.2',
    active: 'primary.3',
    inactive: 'typo.3',
  },
  secondary: {
    default: 'secondary.2',
    hover: 'secondary.3',
    active: 'secondary.1',
    inactive: 'typo.3',
  },
  green: {
    default: 'green.1',
    hover: 'green.2',
    active: 'green.3',
    inactive: 'typo.3',
  },
  red: {
    default: 'red.1',
    hover: 'red.2',
    active: 'red.3',
    inactive: 'typo.3',
  },
  outline: {
    default: 'typo.2',
    hover: 'typo.2',
    active: 'typo.2',
    inactive: 'typo.2',
  },
}

export const buttonTextProps = {
  outline: {
    default: 'primary.1' as ColorsVariant,
    inactive: 'typo.3' as ColorsVariant,
  },
  primary: {
    default: 'typo.2' as ColorsVariant,
    inactive: 'typo.2' as ColorsVariant,
  },
  secondary: {
    default: 'typo.1' as ColorsVariant,
    inactive: 'typo.2' as ColorsVariant,
  },
}

export const outlineBorder = {
  default: 'primary.1' as ColorsVariant,
  inactive: 'typo.3' as ColorsVariant,
}
