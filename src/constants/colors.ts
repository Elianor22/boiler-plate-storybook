export const ColorProps = {

  'typo.1': {
    dark: '#EFEFEF',
    light: '#4C5052',
  },
  'typo.2': {
    dark: '#FFFFFF',
    light: '#394B51',
  },
  'typo.3': {
    dark: '#99A0A0',
    light: '#99A0A0',
  },
  'primary.1': {
    dark: '#A200B5',
    light: '#A200B5'
  },
  'primary.2': {
    dark: '#C400FF',
    light: '#C400FF',
  },
  'primary.3': {
    dark: '#C490E4',
    light: '#C490E4',
  },
  'secondary.1': {
    dark: '#F3F3F3',
    light: '#F3F3F3',
  },
  'secondary.2': {
    dark: '#2B3B40',
    light: '#EEEDED',
  },
  'secondary.3': {
    dark: '#394B51',
    light: '#E8E8E8',
  },
  'green.1': {
    dark: '#01C294',
    light: '#01C294',
  },
  'green.2': {
    dark: '#00CC9B',
    light: '#00CC9B',
  },
  'green.3': {
    dark: '#00A880',
    light: '#00A880',
  },
  'red.1': {
    dark: '#DF4661',
    light: '#DF4661',
  },
  'red.2': {
    dark: '#E94A66',
    light: '#E94A66',
  },
  'red.3': {
    dark: '#CB3F58',
    light: '#CB3F58',
  },
  'white': {
    dark: '#fff',
    light: '#fff',
  }
}

export type ColorsVariant =
  | 'typo.1'
  | 'typo.2'
  | 'typo.3'
  | 'primary.1'
  | 'primary.2'
  | 'primary.3'
  | 'secondary.1'
  | 'secondary.2'
  | 'secondary.3'
  | 'green.1'
  | 'green.2'
  | 'green.3'
  | 'red.1'
  | 'red.2'
  | 'red.3'
  | 'white'
