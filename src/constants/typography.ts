export const TypographyStyle = {
    h1: {
        fontFamily: "Poppins",
        fontWeight: 600,
        fontSize: '24px',
        lineHeight: '36px'
    },
    h2: {
        fontFamily: "Poppins",
        fontWeight: 600,
        fontSize: '20px',
        lineHeight: '30px'
    },
    h3: {
        fontFamily: "Open Sans",
        fontWeight: 700,
        fontSize: '20px',
        lineHeight: '26px'
    },
    h4: {
        fontFamily: "Poppins",
        fontWeight: 600,
        fontSize: '18px',
        lineHeight: '21px'
    },
    body1: {
        fontFamily: 'Poppins',
        fontWeight: 500,
        fontSize: '14px',
        lineHeight: '21px',
    },
    body2: {
        fontFamily: 'Poppins',
        fontWeight: 500,
        fontSize: '12px',
        lineHeight: '18px',
    },
    body3: {
        fontFamily: 'Open Sans',
        fontWeight: 700,
        fontSize: '12px',
        lineHeight: '14px',
    },
    button: {
        fontFamily: 'Open Sans',
        fontWeight: 600,
        fontSize: '12px',
        lineHeight: '14px',
    },
    caption: {
        fontFamily: 'Poppins',
        fontWeight: 500,
        fontSize: '12px',
        lineHeight: '15px',
    }
}

export type TypographyVariant = 'h1' | 'h2' | 'h3' | 'h4' | 'body1' | 'body2' | 'button' | 'caption'