import { SxProps } from "@mui/material";
import { ColorsVariant } from "./colors";

export interface singleIconProps {
    color?: ColorsVariant;
    additionalCss?: SxProps;
    size?: number;
    sx?: SxProps;

}

export interface IconProps {
    variant: IconsVariant;
    color?: ColorsVariant;
    sx?: SxProps;
}

export type IconsVariant =
    'burger' | 'arrowup' | 'arrowdown' | 'arrowleft' | 'arrowright' | 'plus'
