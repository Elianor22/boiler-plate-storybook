import { Meta, Story } from "@storybook/react";
import InputFieldComp from "../../components/stories/InputField";
// import InputOTP from "../../components/molecules/InputField/InputOTP/InputOTP";
import React from "react";

export default {
    title: "Components/Molecules/InputField",
    component: InputFieldComp,
} as Meta;

export const InputField: Story = () => <InputFieldComp />;
// export const InputOtp : Story = () => <InputOTP/>
