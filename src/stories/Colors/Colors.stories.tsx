import { Meta, Story } from "@storybook/react";
import ColorsComp from "../../components/stories/Colors";

export default {
    title: "Colors",
    component: ColorsComp,
} as Meta;

export const Colors: Story = (args) => <ColorsComp {...args} />;
