import { Meta, Story } from "@storybook/react";
import { IButton } from "../../components/atoms/Button/ButtonComponent";
import LogoComponent from "../../components/stories/Logo";

export default {
    title: "Components/Atoms/Logo",
    component: LogoComponent,
    // argTypes: {
    //     variant: {
    //         options: ["large", "medium", "small"],
    //         control: { type: "radio" },
    //     },
    // },
} as Meta;

const Template: Story<IButton> = (args) => <LogoComponent  />;

export const Logo = Template.bind({});
Logo.args = {
    children: "primary button",
    variant: "primary",
    type: "default",
    className: "button",
    onClick: () => console.log("button"),
    width: 200,
    block: false,
    isSubmit: false,
    sx: { display: "flex" },
};
