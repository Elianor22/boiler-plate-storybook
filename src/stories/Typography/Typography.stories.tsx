import { Meta, Story } from "@storybook/react";
import TypographyComp from "../../components/stories/Typography";
import { ITypography } from "../../components/atoms/Typography";
import React from "react";

export default {
    title: "Components/Atoms/Typography",
    component: TypographyComp,
    argTypes: {
        variant: {
            options: [
                "h1",
                "h2",
                "h3",
                "h4",
                "body1",
                "body2",
                "body3",
                "caption",
                "button",
            ],
            control: { type: "radio" },
        },
    },
} as Meta;

 const Template: Story<ITypography> = (args) => (
    <TypographyComp {...args} />
);

export const Typography = Template.bind({});

Typography.args = {
    variant: "body1",
    weight: 500,
    className: "cssName",
    mb: 0,
    mt: 0,
    sx: { textAlign: "center" },
};
