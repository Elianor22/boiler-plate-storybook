import { Meta, Story } from "@storybook/react";
import { IButton } from "../../components/atoms/Button/ButtonComponent";
import ButtonComponent from "../../components/stories/Button";
import React from "react";

export default {
    title: "Components/Atoms/Buttons",
    component: ButtonComponent,
    argTypes: {
        variant: {
            options: ["primary", "secondary", "green", "red", "outline"],
            control: { type: "radio" },
        },
        type: {
            options: ["default", "inactive"],
            control: { type: "radio" },
        },
    },
} as Meta;

const Template: Story<IButton> = (args) => <ButtonComponent {...args} />;

export const Default = Template.bind({});
Default.args = {
    children: "primary button",
    variant: "primary",
    type: "default",
    className: "button",
    onClick: () => console.log("button"),
    width: 200,
    block: false,
    isSubmit: false,
    sx: { display: "flex" },
};
