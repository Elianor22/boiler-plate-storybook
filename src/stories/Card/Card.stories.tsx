import { Meta, Story } from "@storybook/react";
import CardComponent from "../../components/stories/Card";
import React from "react";

export default {
    title: "Components/Molecules/Card",
    component: CardComponent,
} as Meta;
const Template: Story = (args) => <CardComponent {...args} />;
export const Card = Template.bind({});
Card.args = {
    title: "this is card title",
    titleVariant:"h2",
    children: `this is card children can able react node or just string `,
    sxCard: { display: "block" },
};
