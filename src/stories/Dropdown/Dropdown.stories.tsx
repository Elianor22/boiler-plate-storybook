import { Meta, Story } from "@storybook/react";
import DropdownComponent from "../../components/stories/Dropdown";
import React from "react";

export default {
    title: "Components/Molecules/Dropdown",
    component: DropdownComponent,
} as Meta;

export const Dropdown: Story = () => <DropdownComponent />;
