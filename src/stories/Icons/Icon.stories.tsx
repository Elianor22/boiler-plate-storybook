import { Story,ComponentMeta } from "@storybook/react";
import Icon from "../../components/stories/Icon";
import React from "react";

export default {
    title: "Components/Atoms/Icon",
    component: Icon,
} as ComponentMeta<typeof Icon>;
export const Icons: Story = () => <Icon />;
