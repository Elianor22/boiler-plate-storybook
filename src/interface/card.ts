import { SxProps } from "@mui/material";
import { TypographyVariant } from "../constants/typography";

export interface ICard {
    children?: React.ReactNode;
    width?: string | number;
    bgColor?: string;
    title?: string;
    titleVariant?: TypographyVariant;
    sxTitle?: SxProps;
    sxCard?: SxProps;
    sxHeader?: React.CSSProperties;
}