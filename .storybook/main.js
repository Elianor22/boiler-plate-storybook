module.exports = {
    stories: [
        "../src/**/*.stories.mdx",
        "../src/**/*.stories.@(js|jsx|ts|tsx)",
    ],
    addons: [
        "@storybook/addon-links",
        "@storybook/addon-essentials",
        "storybook-dark-mode",
    ],
    framework: "@storybook/react",
    core: {
        builder: "@storybook/builder-vite",
    },
    // webpackFinal: async (config, { configType }) => {
    //     config.output.publicPath = "/storybook/";
    //     return config;
    // },
    // managerWebpack: async (config) => {
    //     config.output.publicPath = "/storybook/";
    //     return config;
    // },
};

// module.exports = {
//     stories: [
//         "../src/**/*.stories.mdx",
//         "../src/**/*.stories.@(js|jsx|ts|tsx)",
//     ],
//     addons: [
//         "@storybook/addon-links",
//         "@storybook/addon-essentials",
//         "@storybook/addon-interactions",
//         "@storybook/preset-create-react-app",
//         "storybook-dark-mode",
//     ],
//     framework: "@storybook/react",
//     core: {
//         builder: "@storybook/builder-vite",
//     },
// };
