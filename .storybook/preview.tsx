import React from "react";
import { globalTheme } from "../src/utils/theme/theme";
import { createTheme } from "@mui/material";
import { useDarkMode } from "storybook-dark-mode";

import { themes } from "@storybook/theming";
import { CssBaseline, ThemeProvider } from "@mui/material";

import'../src/App.css'

export const parameters = {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
        matchers: {
            color: /(background|color)$/i,
            date: /Date$/,
        },
    },
    options: {
        storySort: (a, b) =>
            a[1].kind === b[1].kind
                ? 0
                : a[1].id.localeCompare(b[1].id, undefined, { numeric: true }),
    },
    backgrounds: {
        disable: true,
    },
    darkMode: {
        dark: {
            ...themes.dark,
            appContentBg: "#030E15",
        },
        light: {
            ...themes.normal,
            appContentBg: "#FAFAFA",
        },
    },
};

const withMuiTheme = (Story, { args }) => {
    const darkMode = useDarkMode() || args.theme === "dark";

    const theme = createTheme({
        palette: {
            mode: darkMode ? "dark" : "light",
            ...globalTheme.palette,
        },
        typography: {
            ...globalTheme.typography,
        },
        breakpoints: {
            ...globalTheme.breakpoints,
        },
    });

    return (
        <ThemeProvider theme={theme}>
            <CssBaseline>
                <Story />
            </CssBaseline>
        </ThemeProvider>
    );
};

export const decorators = [withMuiTheme];
